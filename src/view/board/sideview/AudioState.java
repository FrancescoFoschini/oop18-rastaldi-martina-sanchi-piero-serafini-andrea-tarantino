package view.board.sideview;

/**
 * 
 * Piero Sanchi. represents the state of the button "audio" in GameView.
 *
 */
public interface AudioState {

    /**
     * 
     * @param gameview
     *            the view that contains the "audio" button.
     */
    void execute(GameView gameview);

}
